![drag=width, height, drop=x y, stretch=true](https://cdn2.tz.nl/wp-content/uploads/2017/08/MongoDB-1284x856.png)

---

## how to use

```sh
rails new project_name --skip-bundle --skip-active-record --skip-test --skip-system-test
cd project_name

```

```ruby
gem 'mongoid'
gem 'mongoid_rails_migrations'
gem 'simple_enum'
gem 'mongoid-rspec' # model tests

```

```sh
bundle install
rails g mongoid:config
```
---

## Basic api flow:

### Models (no schemas):

```ruby
# frozen_string_literal: true

require 'simple_enum/mongoid'

class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include SimpleEnum::Mongoid

  has_and_belongs_to_many :places, autosave: true
  has_many :orders

  embeds_one :profile
```
---

```ruby

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  field :email,              type: String, default: ''
  field :encrypted_password, type: String, default: ''
  field :name, type: String

  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  field :remember_created_at, type: Time

  as_enum :role, %i[courier admin], field: { type: Integer, default: 0 }

  validates_presence_of :name
end
```

---

## First con - extra migrations gem

```ruby
class Place
  include Mongoid::Document
  field :loc, type: Array # wanna change the name
  field :name, type: String
  field :description, type: String

  has_and_belongs_to_many :users, autosave: true

  index({ location: '2d' }, { min: -200, max: 200 })
end
```
---

```ruby
class RenamePlaceColumn < Mongoid::Migration
  def self.up
    Place.all.rename(loc: :location)
  end

  def self.down
    Place.all.rename(location: :loc)
  end
end
```
And after that we can change name for the model

```sh
rails db:migrate
rails db:mongoid:remove_indexes
rails db:mongoid:create_indexes
```
---

## relations

* has_one;
* belongs_to;
* bla bla bla bla

* **has_and_belongs_to_many** - that one interesting
* embeds_one(many) + embedded_in

---

## has_and_belongs_to_many

user class:
```ruby
class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include SimpleEnum::Mongoid

  has_and_belongs_to_many :places, autosave: true
```

place class:

```ruby
class Place
  include Mongoid::Document

  has_and_belongs_to_many :users, autosave: true
```

---

```
[1] pry(main)> user = FactoryBot.create :user
=> #<User _id: BSON::ObjectId('5f9acfd058b42a5e5b8d84b0'), created_at: Thu, 29 Oct 2020 14:21:04 UTC +00:00, email: "walter.gibson@schaden.org", name: "Vance Rempel", place_ids: [], role_cd: 0, updated_at: Thu, 29 Oct 2020 14:21:04 UTC +00:00>
[2] pry(main)> place = FactoryBot.create :place
=> #<Place _id: 5f9acfdc58b42a5e5b8d84b1, location: [85.64769438332911, 3.572694814328429], name: "Brazilian Space Agency", description: "I'll transmit the haptic SMTP pixel, that should interface the XSS monitor!",
user_ids: nil>
```
---
```
[6] pry(main)> user.update(places: [place])
=> true
[7] pry(main)> user.places
=> [#<Place _id    "updated_at": {
        "$date": "2020-10-29T14:21:04.960Z"
    },
    "created_at": {
        "$date": "2020-10-29T14:21:04.960Z"
    },
: 5f9acfdc58b42a5e5b8d84b1, location: [85.64769438332911, 3.572694814328429], name: "Brazilian Space Agency", description: "I'll transmit the haptic SMTP pixel, that should interface the XSS monitor!", user_ids: [BSON::ObjectId('5f9acfd058b42a5e5b8d84b0')]>]
```
---
```
[8] pry(main)> place.users
=> [#<User _id: BSON::ObjectId('5f9acfd058b42a5e5b8d84b0'), created_at: Thu, 29 Oct 2020 14:21:04 UTC +00:00, email: "walter.gibson@schaden.org", name: "Vance Rempel", place_ids: [BSON::ObjectId('5f9acfdc58b42a5e5b8d84b1')], role_cd: 0, updated_at: Thu, 29 Oct 2020 14:21:04 UTC +00:00>]
```
---

MongoDB json looks like

```json
//place
{
    "_id": {
        "$oid": "5f9acfdc58b42a5e5b8d84b1"
    },
    "name": "Italian Space Agency",
    "description": "We need to bypass the auxiliary SCSI circuit!",
    "location": [-21.59578064710007, -75.99886937171243],
    "user_ids": [{
        "$oid": "5f9acfd058b42a5e5b8d84b0"
    }]
}
```
---

```json
//user
{
    "_id": {
        "$oid": "5f9acfd058b42a5e5b8d84b0"
    },
    "place_ids": [{
        "$oid": "5f9acfdc58b42a5e5b8d84b1"
    }]
}
```

---

embedded entities

```ruby
class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include SimpleEnum::Mongoid

  embeds_one :profile

```

```ruby
class Profile
  include Mongoid::Document
  field :phone, type: String
  field :email, type: String

  embedded_in :user
end
```
---

```sh
[9] pry(main)> Profile.create(
phone: '88005553535', email: 'asdasd@asd.com', user: user)
=> #<Profile _id: 5f9ad19a58b42a5e5b8d84b2,
phone: "88005553535", email: "asdasd@asd.com">
```
---
profile inside the user


```json
{
  "_id": {
      "$oid": "5f9acfd058b42a5e5b8d84b0"
  },
  "email": "walter.gibson@schaden.org",
  "encrypted_password": "$2a$12$7EMIR6b28UbM1l2Ogt9cd.XBrXzDo6U2OvgabEpc2aJolrASp2.ea",
  "role_cd": 0,
  "place_ids": [{
      "$oid": "5f9acfdc58b42a5e5b8d84b1"
  }],
  "profile": {
      "_id": {
          "$oid": "5f9ad19a58b42a5e5b8d84b2"
      },
      "phone": "88005553535",
      "email": "asdasd@asd.com"
  },
  "name": "Vance Rempel",
  "updated_at": {
      "$date": "2020-10-29T14:21:04.960Z"
  },
  "created_at": {
      "$date": "2020-10-29T14:21:04.960Z"
  },
})
```

---

## Sessions + transactions

```ruby
class Orders::TakeOne
  def call(user:, order_id:)
    place_ids = user.places.pluck(:_id)
    order = Order.open.where(place_id: place_ids).find(order_id)

    proceed_assiging(user, order)

    order
  end
  #next slide
end
```

---

```ruby
def proceed_assiging(user, order)
  order.with_session do |session|
    session.start_transaction
    order.user_id = user.id
    order.deliver!
    session.commit_transaction
  end
end
```

---

### fast locking less explaining

```ruby
Order
  .open
  .where(id: order_id)
  .find_one_and_update({
    '$set': { user_id: user.id, status: :delivering },
    return_document: :after
  })
```

---

## Type of locks

![](https://www.tutorialkart.com/wp-content/uploads/2018/02/mongodb_locks.png)

---

## Geo Search

```ruby
class Place
  include Mongoid::Document
  field :location, type: Array
  field :name, type: String
  field :description, type: String

  has_and_belongs_to_many :users, autosave: true

  index({ location: '2d' }, { min: -200, max: 200 })
end
```
---

```ruby
class Places::AssignToUser
  def call(user:, coords:)
    place = Place.where(
      location: {
        '$near': coords,
        '$maxDistance': 5,
        '$minDistance': 0
      }
    ).first

    user.places << place
  end
end
```
---
## Data serializing

```ruby
def index
  place.orders.open.to_json
end
```
---

![](http://images.shoutwiki.com/ytp/thumb/f/fb/Vrunishka.png/300px-Vrunishka.png)


---
User with embedded profile

```ruby
class UserSerializer < BaseSerializer
  has_one :profile, &:profile

  attributes :name, :email, :role
end
```
---

```json
{
    "data": {
        "id": "5f99627c58b42a0f4a4cd5f6",
        "type": "user",
        "attributes": {
            "name": "John Lynch MD",
            "email": "mrjordanvl@gmail.com",
            "role": "courier"
        },
        "relationships": {
            "profile": {
                "data": {
                    "id": "5f9bd42158b42a115b5caa33",
                    "type": "profile"
                }
            }
        }
    }}

```
---

```json
"included": [
        {
            "id": "5f9bd42158b42a115b5caa33",
            "type": "profile",
            "attributes": {
                "phone": "MyString",
                "email": "MyString"
            }
        }
    ]
```
---
Conclusion
